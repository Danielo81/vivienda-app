import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Persona } from './clases/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  private urlPersonas = 'http://localhost:9090/vivienda/getPersonas';
  private urlAgregarPersona ='http://localhost:9090/persona/createPersona'

  constructor(private http: HttpClient) { }

  getPersona(id: number):Observable<any>{
    return this.http.get(`${this.urlPersonas}/${id}`);
  }

  createPersona(persona: Object): Observable<Object> {
    return this.http.post(`${this.urlAgregarPersona}`, persona);
  }

  updatePersona(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.urlPersonas}/${id}`, value);
  }

  eliminarPersona(persona: Persona): Observable<any> {
    return this.http.post(`${this.urlPersonas}`,persona);
  }

  getPersonas(): Observable<any> {
    return this.http.get(`${this.urlPersonas}`);
  }



}
