import { TipoDocumento } from "./tipoDocumento";
import { Pais } from "./Pais";

export class Persona {
    
    id: number;
    nombres: string;
    apellidos: string;
    numeroDocumento: number;
    genero: string;
    edad: 39;
    tipoDocumento: TipoDocumento;
    pais: Pais;

}