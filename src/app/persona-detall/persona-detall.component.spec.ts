import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaDetallComponent } from './persona-detall.component';

describe('PersonaDetallComponent', () => {
  let component: PersonaDetallComponent;
  let fixture: ComponentFixture<PersonaDetallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonaDetallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaDetallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
