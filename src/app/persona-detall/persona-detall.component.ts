import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonaService } from '../persona.service';
import { Persona } from '../clases/persona';
import { PersonaListaComponent } from '../persona-lista/persona-lista.component';

@Component({
  selector: 'app-persona-detall',
  templateUrl: './persona-detall.component.html',
  styleUrls: ['./persona-detall.component.css']
})
export class PersonaDetallComponent implements OnInit {

  id: number;
  persona: Persona;

  constructor(private route: ActivatedRoute,private router: Router,
    private personaService: PersonaService) { }
  

  ngOnInit(): void {
    this.persona = new Persona();
    this.id = this.route.snapshot.params['id'];

    this.personaService.getPersona(this.id)
      .subscribe(data => {
        console.log(data)
        this.persona = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['personas']);
  }

}
