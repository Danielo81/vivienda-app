import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Persona} from '../clases/persona'
import { PersonaService } from '../persona.service';

@Component({
  selector: 'app-update-persona',
  templateUrl: './update-persona.component.html',
  styleUrls: ['./update-persona.component.css']
})
export class UpdatePersonaComponent implements OnInit {

  persona : Persona;
  id: number;
  submitted = false;


  constructor(private route: ActivatedRoute,private router: Router,private personaService : PersonaService) { }

  ngOnInit(): void {
    this.persona = new Persona();

    this.id = this.route.snapshot.params['id'];

    this.personaService.getPersona(this.id)
      .subscribe(data => {
        console.log(data)
        this.persona = data;
      }, error => console.log(error));
  }

  updatePersona() {
    this.personaService.updatePersona(this.id, this.persona)
      .subscribe(data => console.log(data), error => console.log(error));
    this.persona = new Persona();
    this.gotoList();
  }

  onSubmit() {
    this.updatePersona();  
    this.submitted = true;  
  }

  gotoList() {
    this.router.navigate(['/personas']);
  }


}
