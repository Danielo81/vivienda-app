import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreatePersonaComponent } from './create-persona/create-persona.component';
import { PersonaDetallComponent } from './persona-detall/persona-detall.component';
import { PersonaListaComponent } from './persona-lista/persona-lista.component';
import { UpdatePersonaComponent } from './update-persona/update-persona.component';

@NgModule({
  declarations: [
    AppComponent,
    CreatePersonaComponent,
    PersonaDetallComponent,
    PersonaListaComponent,
    UpdatePersonaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
