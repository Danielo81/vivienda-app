import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { Router } from '@angular/router';
import { Persona} from '../clases/persona'
import { PersonaService } from '../persona.service';


@Component({
  selector: 'app-persona-lista',
  templateUrl: './persona-lista.component.html',
  styleUrls: ['./persona-lista.component.css']
})
export class PersonaListaComponent implements OnInit {
  personas: Observable<Persona[]>

  constructor( private personaService : PersonaService, private router: Router) { }

  ngOnInit(): void {
    this.recargaDatos();
  }

  recargaDatos(){
    this.personas = this.personaService.getPersonas();
  }

  eliminarPersona(persona: Persona){
    this.personaService.eliminarPersona(persona).subscribe(
      data => {
        console.log(data);
        this.recargaDatos();
      },error => console.log(error));
    };

    getPersonaDetalle(id: number){
      this.router.navigate(['detalles',id]);      
    }

  
  }
