import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from '../clases/persona';
import { PersonaService} from '../persona.service';

@Component({
  selector: 'app-create-persona',
  templateUrl: './create-persona.component.html',
  styleUrls: ['./create-persona.component.css']
})
export class CreatePersonaComponent implements OnInit {

  persona : Persona = new Persona();
  submitted = false;

  constructor(private personaService : PersonaService, private router: Router) { }

  nuevoEmpleado(): void {
    this.submitted = false;
    this.persona = new Persona();
  }

  guardar(){
    this.personaService.createPersona(this.persona)
    .subscribe( data => console.log(data), error => console.log(error));
    this.persona = new Persona();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.guardar();    
  }

  gotoList() {
    this.router.navigate(['/personas']);
  }

  ngOnInit(): void {
  }

}
