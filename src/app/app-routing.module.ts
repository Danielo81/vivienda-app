import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonaDetallComponent } from './persona-detall/persona-detall.component';
import { CreatePersonaComponent } from './create-persona/create-persona.component';
import { PersonaListaComponent } from './persona-lista/persona-lista.component';
import { UpdatePersonaComponent } from './update-persona/update-persona.component';

const routes: Routes = [
  { path: '', redirectTo: 'employee', pathMatch: 'full' },
  { path: 'personas', component: PersonaListaComponent },
  { path: 'add', component: CreatePersonaComponent },
  { path: 'update/:id', component: UpdatePersonaComponent },
  { path: 'detalles/:id', component: PersonaDetallComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
